namespace :exchange do
  desc "TODO"
  task get_from_api: :environment do
    start_time = Time.current()
    exchange_getting_logger = Logger.new("#{Rails.root}/log/exchange_getting.log")
    exchange_getting_logger.info("\n" + "*" * 15 + "\nGetting new exchange\n" + "*" * 15)

    exchanges = Array.new
    json = get_json

    time = Time.now.to_formatted_s(:db)
    group = json['payload']['lastUpdate']['milliseconds']

    json['payload']['rates'].each do |ex|
      exchanges << prepare_data(ex, time, group)
    end

    Exchange.create(exchanges)

    lead_time = Time.current() - start_time

    exchange_getting_logger.info("\n" + "*" * 15 + "\nGetting new exchange completed in #{lead_time}s\n" + "*" * 15)
  end

  def get_json
    begin
      require 'open-uri'
      api_url = 'https://www.tinkoff.ru/api/v1/currency_rates/'
      return json = JSON.load(open(api_url))
    rescue Exception => e
      exchange_getting_logger.warn(e.message)
    end
  end

  def prepare_data(exchange, time, group)
    category_id = Category.find_or_create_by(name: exchange['category']).id
    from_currency_id = Currency.create_with(code: exchange['fromCurrency']['name'].downcase)
      .find_or_create_by(name: exchange['fromCurrency']['name']).id
    to_currency_id = Currency.create_with(code: exchange['toCurrency']['name'].downcase)
        .find_or_create_by(name: exchange['toCurrency']['name']).id
    buy = exchange['buy']
    sell ||= (exchange['sell'] ||= 0)

    e = {time: time, group: group, category_id: category_id, from_currency_id: from_currency_id, to_currency_id: to_currency_id, buy: buy, sell: sell}
  end
end
