namespace :trigger do
  desc "Destroying notificated triggers"

  task destroy_notificated: :environment do
    notificated_triggers = Trigger.where(is_notified: true).order('created_at ASC').limit(100)
    notificated_triggers.map do |tr|
      tr.destroy
    end
  end

end
