# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161016103449) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "currencies", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "exchanges", force: :cascade do |t|
    t.datetime "time"
    t.integer  "group",            limit: 8
    t.decimal  "buy",                        precision: 5, scale: 2
    t.decimal  "sell",                       precision: 5, scale: 2
    t.integer  "category_id"
    t.integer  "from_currency_id"
    t.integer  "to_currency_id"
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
  end

  add_index "exchanges", ["category_id"], name: "index_exchanges_on_category_id", using: :btree
  add_index "exchanges", ["from_currency_id"], name: "index_exchanges_on_from_currency_id", using: :btree
  add_index "exchanges", ["group"], name: "index_exchanges_on_group", using: :btree
  add_index "exchanges", ["to_currency_id"], name: "index_exchanges_on_to_currency_id", using: :btree

  create_table "sessions", force: :cascade do |t|
    t.string   "s_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "sessions", ["s_id"], name: "index_sessions_on_s_id", unique: true, using: :btree

  create_table "triggers", force: :cascade do |t|
    t.integer  "category_id"
    t.integer  "session_id"
    t.integer  "from_currency_id"
    t.integer  "to_currency_id"
    t.string   "comparison"
    t.string   "currency_operation_type"
    t.string   "email"
    t.decimal  "overstep_value",          precision: 5, scale: 2
    t.boolean  "is_fired",                                        default: false, null: false
    t.boolean  "is_notified",                                     default: false, null: false
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
  end

  add_index "triggers", ["category_id"], name: "index_triggers_on_category_id", using: :btree
  add_index "triggers", ["from_currency_id"], name: "index_triggers_on_from_currency_id", using: :btree
  add_index "triggers", ["session_id"], name: "index_triggers_on_session_id", using: :btree
  add_index "triggers", ["to_currency_id"], name: "index_triggers_on_to_currency_id", using: :btree

  add_foreign_key "exchanges", "categories"
  add_foreign_key "exchanges", "currencies", column: "from_currency_id"
  add_foreign_key "exchanges", "currencies", column: "to_currency_id"
  add_foreign_key "triggers", "categories"
  add_foreign_key "triggers", "currencies", column: "from_currency_id"
  add_foreign_key "triggers", "currencies", column: "to_currency_id"
  add_foreign_key "triggers", "sessions"
end
