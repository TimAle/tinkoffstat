class CreateExchanges < ActiveRecord::Migration
  def change
    create_table :exchanges do |t|
      t.datetime :time
      t.integer :group, limit: 8, index: true
      t.decimal :buy, precision: 5, scale: 2
      t.decimal :sell, precision: 5, scale: 2
      t.references :category, index: true, foreign_key: true
      t.references :from_currency, references: :currencies, index: true
      t.references :to_currency, references: :currencies, index: true

      t.timestamps null: false
      end

      add_foreign_key :exchanges, :currencies, column: :from_currency_id
      add_foreign_key :exchanges, :currencies, column: :to_currency_id
    end
end
