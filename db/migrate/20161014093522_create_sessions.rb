class CreateSessions < ActiveRecord::Migration
  def change
    create_table :sessions do |t|
      t.string :s_id

      t.timestamps null: false
    end
    add_index :sessions, :s_id, unique: true
  end
end
