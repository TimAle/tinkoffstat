class CreateTriggers < ActiveRecord::Migration
  def change
    create_table :triggers do |t|
      t.references :category, index: true, foreign_key: true
      t.references :session, index: true, foreign_key: true
      t.references :from_currency, references: :currencies, index: true
      t.references :to_currency, references: :currencies, index: true
      t.string :comparison
      t.string :currency_operation_type
      t.string :email
      t.decimal :overstep_value, precision: 5, scale: 2
      t.boolean :is_fired, null: false, default: false
      t.boolean :is_notified, null: false, default: false

      t.timestamps null: false
    end

    add_foreign_key :triggers, :currencies, column: :from_currency_id
    add_foreign_key :triggers, :currencies, column: :to_currency_id
  end
end
