const nodePath = require('path');
const gulp = require('gulp');
const gulplog = require('gulplog');
const less = require('gulp-less');
const sass = require('gulp-sass');
const merge = require('stream-merger');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const concat = require('gulp-concat');
const babel = require('gulp-babel');
const newer = require('gulp-newer');
const browserSync = require('browser-sync').create();
const combine = require('stream-combiner2').obj;
const notify = require('gulp-notify');
const notifier = require('node-notifier');
const eslint = require('gulp-eslint');
const through2 = require('through2').obj;
const fs = require('fs');
const gulpIf = require('gulp-if');
const AssetsPlugin = require('assets-webpack-plugin');
const webpack = require('webpack');

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

const path = {
    build: {
        //Тут мы укажем куда складывать готовые после сборки файлы
        js: 'public/assets/js/',
        css: 'public/assets/css/',
        img: 'public/assets/img/'
    },
    src: { //Пути откуда брать исходники
        js: 'app/assets/javascripts/**/*.js',
        less: 'app/assets/stylesheets/**/*.less',
        css: 'app/assets/stylesheets/**/*.css',
        scss: 'node_modules/**/sweetalert.scss',
        img: 'app/assets/images/**/*.*'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        js: 'app/assets/javascripts/**/*.js',
        less: 'app/assets/stylesheets/**/*.less',
        img: 'app/assets/images/**/*.*'
    }
};

gulp.task('styles', function() {
    const lessStream = gulp.src(path.src.less)
      // .pipe(sourcemaps.init())
      .pipe(less().on('error', notify.onError()));

    const scssStream = gulp.src(path.src.scss)
      // .pipe(sourcemaps.init())
      .pipe(sass().on('error', notify.onError()));

    const cssStream = gulp.src(path.src.css);

    return merge([lessStream, scssStream, cssStream])
      .pipe(concat('styles.css'))
      // .pipe(sourcemaps.write())
      .pipe(gulp.dest(path.build.css));
});

gulp.task('js', () => {
    return combine(
        gulp.src(path.src.js),
        sourcemaps.init(),
        babel({
            presets: ['es2015']
        }),
        concat('bundle.js'),
        sourcemaps.write(),
        gulp.dest(path.build.js)
    ).on('error', notify.onError());
});

gulp.task('images', function() {
    return combine(
        gulp.src(path.src.img, {
            since: gulp.lastRun('images')
        }),
        newer(path.build.img),
        gulp.dest(path.build.img)
    ).on('error', notify.onError());
});

gulp.task('webpack', function(callback) {

  let options = {
    entry:   {
      bundle: './app/assets/javascripts/main',
      jquery: './app/assets/javascripts/head',
      bootstrap: './app/assets/javascripts/bootstrap.min'
    },
    output:  {
      path:     __dirname + '/public/assets/js',
      publicPath: 'assets/js/',
      filename: isDevelopment ? '[name].js' : '[name]-[chunkhash:10].js'
    },
    watch:   isDevelopment,
    devtool: isDevelopment ? 'cheap-module-inline-source-map' : null,
    module:  {
      loaders: [{
        test:    /\.js$/,
        include: nodePath.join(__dirname, "app/assets/javascripts"),
        loader:  'babel?presets[]=es2015'
      }]
    },
    plugins: [
      new webpack.NoErrorsPlugin(), // otherwise error still gives a file
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "window.jQuery": "jquery"
      })
    ]
  };

  webpack(options, function(err, stats) {
    if (!err) { // no hard error
      // try to get a soft error from stats
      err = stats.toJson().errors[0];
    }

    if (err) {
      notifier.notify({
        title: 'Webpack',
        message: err
      });

      gulplog.error(err);
    } else {
      gulplog.info(stats.toString({
        colors: true
      }));
    }

    // task never errs in watch mode, it waits and recompiles
    if (!options.watch && err) {
      callback(err);
    } else {
      callback();
    }

  });


});

gulp.task('build', gulp.parallel(
    'styles',
    'webpack',
    'images'
));

gulp.task('default', gulp.series('styles', 'js', 'images'));

gulp.task('watch', function() {
    gulp.watch(path.watch.less, gulp.series('styles'));
    gulp.watch(path.watch.img, gulp.series('images'));
});

gulp.task('serve', function() {
    browserSync.init({
        proxy: "http://localhost:3000/"
    });
    browserSync.watch('public/**/*.*').on('change', browserSync.reload);
});

gulp.task('dev',
    gulp.series('build', gulp.parallel('watch', 'serve'))
);
