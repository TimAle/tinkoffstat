class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  #protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }

  def current_session
    if cookies[:s_id]
      session ||= Session.find_by(s_id: cookies[:s_id])
    else
      cookies[:s_id] = SecureRandom.uuid
      session = Session.create(s_id: cookies[:s_id])
    end
  end
end
