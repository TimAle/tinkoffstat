class TriggerController < ApplicationController

  skip_before_filter :verify_authenticity_token, only: [:get_user_triggers, :save]
  before_filter :init_session, except: :destroy

  def get_user_triggers
    triggers = Trigger.not_fired.where(session_id: @session.id).includes(:from_currency)
    render partial: "welcome/trigger", collection: triggers || "У вас нет невыполненных триггеров"
  end

  def save
    # захардкодил валюту и категорию
    to_currency_code = 'rub'
    category_name = 'DebitCardsTransfers'

    from_currency_id = request['from_currency']
    to_currency_id = Currency.find_by(code: to_currency_code).id
    overstep_value = (request['rub'] + "." + request['kop']).to_f
    category_id = Category.find_by(name: category_name).id
    currency_operation_type = request['currency_operation_type']
    comparison = request['comparison']
    email = request['email']

    new_trigger = Trigger.new(
      category_id: category_id,
      session_id: @session.id,
      from_currency_id: from_currency_id,
      to_currency_id: to_currency_id,
      comparison: comparison,
      currency_operation_type: currency_operation_type,
      email: email,
      overstep_value: overstep_value
    )

    begin
      new_trigger.save
    rescue
      render json: {
        'text' => 'Не удалось сохранить триггер, пропробуйте снова',
        'class' => 'alert-error'
      }
    else
      render json: {
        'text' => 'Ваш триггер успешно сохранён',
        'class' => 'alert-success'
      }
    end
  end

  def destroy
    trigger_id = request['trigger_id']
    trigger = Trigger.find(trigger_id)
    trigger.destroy

    render text: 'Trigger destroyed'
  end

  private

  def init_session
    @session = current_session
  end

end
