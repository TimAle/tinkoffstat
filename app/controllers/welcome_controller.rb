class WelcomeController < ApplicationController

  before_filter :init_session

  caches_action :index, expires_in: GLOBAL_CACHE_TIME

  def index
    @currencies = Currency.where("name = 'USD' OR name = 'EUR'")

    last_exchanges_and_avgs =  Exchange.get_last_exchanges_and_avgs

    @avgs_per_day = last_exchanges_and_avgs['avgs_per_day']
    @last_exchanges = last_exchanges_and_avgs['last_exchanges']
  end

  private

  def init_session
    @session = current_session
  end

end
