class Trigger < ActiveRecord::Base
  belongs_to :category
  belongs_to :session
  belongs_to :from_currency, class_name: 'Currency'
  belongs_to :to_currency, class_name: 'Currency'

  scope :not_fired, -> { where(is_fired: false) }

  def fire
    TriggerMailer.notify(self).deliver_now
  end

end
