class Exchange < ActiveRecord::Base
  belongs_to :category
  belongs_to :from_currency, class_name: 'Currency'
  belongs_to :to_currency, class_name: 'Currency'


  after_commit :add_to_exchange_job, on: :create

  def self.get_average_per_day(from_currency_id, to_currency_id, category_id)
    date = Date.today
    timezone = ActiveSupport::TimeZone['Moscow']
    start_of_day = timezone.local(date.year, date.month, date.day)

    exchanges_per_day = self.where("
      from_currency_id = :from_currency_id
      AND to_currency_id = :to_currency_id
      AND category_id = :category_id
      AND time >= :start_of_day
    ", {
      from_currency_id: from_currency_id,
      to_currency_id: to_currency_id,
      category_id: category_id,
      start_of_day: start_of_day
    })

    avg_sell = exchanges_per_day.average(:sell)
    avg_buy = exchanges_per_day.average(:buy)

    return {
      from_currency_id => {
        "avg_sell" => avg_sell,
        "avg_buy" => avg_buy
      }
    }
  end

  def self.get_last_exchanges_and_avgs
    last_group = Exchange.last.group
    last_group_exchanges = Exchange.where(group: last_group)
    last_exchanges = Array.new
    avgs_per_day = Hash.new
    # захардкодили валюты и категорию, по идее геттер должен принимать их аргументами
    from_currency_code = ['usd', 'eur']
    to_currency_code = 'rub'
    category_name = 'DebitCardsTransfers'

    from_currency_code.each do |code|

      from_currency_id = Currency.find_by(code: code).id
      to_currency_id = Currency.find_by(code: to_currency_code).id
      category_id = Category.find_by(name: category_name).id

      last_exchange = last_group_exchanges.find_by(
        from_currency_id: from_currency_id,
        to_currency_id: to_currency_id,
        category_id: category_id
      )
      avgs_this_currency = Exchange.get_average_per_day(from_currency_id, to_currency_id, category_id)

      avgs_per_day.merge!(avgs_this_currency)
      last_exchanges.push(last_exchange)
    end
    return {
      'avgs_per_day' => avgs_per_day,
      'last_exchanges' => last_exchanges
    }
  end

  private

  def add_to_exchange_job
    ExchangeValueTriggerJob.perform_later self
  end

end
