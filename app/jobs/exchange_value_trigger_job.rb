class ExchangeValueTriggerJob < ActiveJob::Base
  queue_as :triggers

  def perform(ex)
    triggers = select_actual_triggers(ex)

    triggers.map do |tr|
      tr.update(is_fired: true)
      TriggerNotifyJob.perform_later tr
    end
  end

  def select_actual_triggers(ex)
    actual_triggers = Trigger.not_fired.where("
      category_id = :category_id
      AND from_currency_id = :from_currency_id
      AND to_currency_id = :to_currency_id
      AND ((currency_operation_type = 'buy'
          AND ((comparison = 'gt'
                AND overstep_value < :exchange_buy)
            OR (comparison = 'lt'
                AND overstep_value > :exchange_buy)))
      OR (currency_operation_type = 'sell'
          AND ((comparison = 'gt'
                AND overstep_value < :exchange_sell)
            OR (comparison = 'lt'
                AND overstep_value > :exchange_sell))))
      ", {
      category_id: ex.category_id,
      from_currency_id: ex.from_currency_id,
      to_currency_id: ex.to_currency_id,
      exchange_sell: ex.sell,
      exchange_buy: ex.buy
    })

    return actual_triggers
  end

end
