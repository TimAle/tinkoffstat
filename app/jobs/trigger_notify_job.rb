class TriggerNotifyJob < ActiveJob::Base
  queue_as :trigger_notify_queue

  after_perform do |job|
     trigger = job.arguments.first
     trigger.update(is_notified: true)
  end

  def perform(trigger)
    trigger.fire
  end

end
