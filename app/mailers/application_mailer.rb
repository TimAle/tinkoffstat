class ApplicationMailer < ActionMailer::Base
  default from: "notifications@tinkoffstat.com"
  layout 'mailer'
end
