class TriggerMailer < ApplicationMailer

  default from: 'notifications@tinkoffstat.com'

  def notify(trigger)
    @trigger = trigger
    mail(to: @trigger.email, subject: 'Trigger fired!')
  end
end
