import { get_user_triggers } from './user_triggers_list'

export default function send_trigger_form(form) {

    var msg = form.serialize();
    $.ajax({
        type: 'POST',
        url: 'send_trigger_form',
        data: msg,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        },
        dataType: 'json',
        success: function(data) {
            $('#save-notification').removeAttr('id')
                .before(`<div id="save-notification" class="alert ${data['class']}" style="display: none;">${data['text']}</div>`);
            $('#save-notification').fadeIn(50)
                .delay(1000)
                .fadeOut(600);
        },
        error: function(xhr, str) {
            console.log('Error: ' + xhr.responseCode);
        }
    });

    $(document).ajaxStart(
        function() {
            $('#trigger_form input[type="submit"]').prop('disabled', true);
        }
    );

    $(document).ajaxStop(
        function() {
            $('#trigger_form input[type="submit"]').prop('disabled', false);
        }
    );

    get_user_triggers();
}
