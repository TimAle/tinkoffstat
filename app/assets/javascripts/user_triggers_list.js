export function get_user_triggers() {
    $('.triggers-list').empty();

    $.ajax({
        type: 'POST',
        url: '/get_user_triggers',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        },
        dataType: 'html',
        success: function(data) {
            $('.triggers-list').html(data);
        }
    });

    $(document).ajaxStart(
        function() {
            $('.ajax-loader').show();
        }
    );

    $(document).ajaxStop(
        function() {
            $('.ajax-loader').hide();
        }
    );
}

export function destroy_trigger(trigger_id) {
    $('.triggers-list').empty();

    $.ajax({
        type: 'POST',
        url: 'destroy_trigger',
        data: {
            trigger_id: trigger_id
        },
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        },
        success: function(data) {
            get_user_triggers();
        }
    });

    $(document).ajaxStart(
        function() {
            $('.ajax-loader').show();
        }
    );

    $(document).ajaxStop(
        function() {
            $('.ajax-loader').hide();
        }
    );
}
