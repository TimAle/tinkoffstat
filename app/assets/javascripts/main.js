import send_trigger_form from './trigger_form'
import {
    get_user_triggers,
    destroy_trigger
} from './user_triggers_list'

(function() {
    $(document).ready(function() {
        get_user_triggers();
    })

    $(document).on('submit', '#trigger_form', function() {
        send_trigger_form($('#trigger_form'));
    });

    $(document).on('click', '.triggers-list-update', function() {
        get_user_triggers();
    });

    $(document).on('click', '.destroy-trigger', function() {
        destroy_trigger($(this).data('id'));
    });

})();
